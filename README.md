***

# Synapsis Video Chat directive

***

## Dependencies
* AngularJS 1.5.8
* Angular-Route 1.5.8
* http-server ^0.9.0
* Node 4.4.7
* Simple WebRTC V2 Api

***

## How to use independently
To install dependencies just `npm install`

Afterwards, to start http-server just `npm start`

***

## How to include as a module

1. Make all necessary files available in your index
2. Import angular.module SynapsisVideoChat to your main module (or any module)
3. Use `<video-directive></video-directive>` html syntax to use the directive

	### Parameters

	The `<video-directive></video-directive>` element can receive the following parameters:

	* user [object] = That takes the logged user
	* muted [boolean] = To start the room with audio on/off
	* min-width [integer] = To state the minimal width of video elements
	* invite-handler [method] = Callback triggered with sendInvite method
	* leave-handler [method] = Callback triggered with leaveRoom method
	* start-handler [method] = Callback triggered with videoAdded event
	* max-num-peers [integer] = To define the max number of people allowed in a single room

***

## File tree

```
--app/
|	-- bower_components/
|	-- components/
|	|	--Main/
|	|	|	-- controller.js
|	|	|	-- main.html
|	|	--SimpleWebRTC/
|	|	|	-- latest-v2.js
|	|	--Video_Chat/
|	|	|	-- video_directive.js
|	|	|	-- videoDirective.html
|	-- app.css
|	-- app.js
|	-- BroadcastService.js
|	-- index.html
|	-- module_definitions.js
--e2e-tests/
--node_modules/
--bower.json
--karma.conf.js
--LICENSE
--package.json
--README.md
```