'use strict';

(function(){
    // Declare app level module which depends on views, and components
    angular.module('SynapsisVideoChat', [
    'SynapsisVideoChat.controllers',
    'SynapsisVideoChat.services',
    'SynapsisVideoChat.directives',
    'ngRoute'
    ])
    .config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider
        .when("/", {
            templateUrl:'components/Main/main.html',
            controller: 'MainCtrl',
            controllerAs: 'main'
        })
        .otherwise(
            {redirectTo: '/'}
        );
    }]);
})();
