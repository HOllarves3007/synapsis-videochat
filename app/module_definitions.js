'use strict';

angular.module('SynapsisVideoChat.directives', []);
angular.module('SynapsisVideoChat.services', []);
angular.module('SynapsisVideoChat.controllers', []);
