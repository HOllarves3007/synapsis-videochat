'use strict';

(function(){

    angular.module('SynapsisVideoChat.directives')
        .directive('videoChat', function(){
            return {
                restrict: 'AE',
                transclude: true,
                templateUrl:'components/Video_Chat/video-directive.html',
                scope:{
                    user: '@',
                    muted: '=',
                    minWidth: '=',
                    inviteHandler: '&',
                    leaveHandler: '&',
                    startHandler: '&',
                    maxNumPeers:'='
                },
                link: function(scope, el, attrs){
                    scope.mirror = attrs.mirror === 'true';
                    scope.muted = attrs.muted === 'true';
                },
                controller: function($scope, $rootScope, BroadcastService){

                    //Parsing user
                    $scope.userObj = JSON.parse($scope.user);

                    var webrtc;
                    //Creating options object
                    function formRTCOptions(){
                        var webRTCOptions = {
                            // the id/element dom element that will hold "our" video
                            localVideoEl: 'localVideo',
                            // the id/element dom element that will hold remote videos
                            remoteVideosEl: '',
                            // immediately ask for camera access
                            autoRequestMedia: true,
                            //debugging capabilities
                            debug: false,
                            //media properties
                            media:{
                                audio: true,
                                video: true
                            }
                        };

                        //If muted is true
                        if ($scope.muted) {
                            console.log('Muting!');
                            webRTCOptions.media = {
                                audio: false,
                                video: true
                            };
                        }

                        //If width was specified
                        if ($scope.minWidth) {
                            var minWidth = parseInt($scope.minWidth);
                            if (typeof webRTCOptions.media.video !== 'object') {
                                webRTCOptions.media.video = {};
                            }
                            webRTCOptions.media.video.mandatory = {
                                minWidth: minWidth,
                                maxWidth: minWidth
                            };
                        }
                        return webRTCOptions;
                    }

                    //When prepare signal is received we instantiate the object
                    $scope.$on('prepare', function prepareToBroadcast() {

                        if (webrtc) {
                            console.log('already has prepared');
                            return;
                        }

                        //Creating options object
                        var webRTCOptions = formRTCOptions();
                        //Instantiating webrtc object
                        webrtc = new SimpleWebRTC(webRTCOptions);
                        //Passing additional parameters to the already existent WebRTC object
                        postCreateRTCOptions(webrtc);
                        //Passing webrtc object to $rootScope
                        $rootScope.webrtc = webrtc;
                        //Initializing event handlers
                        rtcEventHandler(webrtc);

                        //Creating room name
                        BroadcastService.room = BroadcastService.host.name + '-' + BroadcastService.receiver.name;
                        console.log('joining room = ', BroadcastService.room);
                        //Joining room
                        webrtc.joinRoom(BroadcastService.room);
                        console.log('Room created', BroadcastService);

                        //Sending Invite
                        if($scope.inviteHandler){
                            $scope.inviteHandler(BroadcastService);
                        }

                        //User is prepared, displaying chat
                        $scope.isPrepared = true;

                    });

                    //Function to initialize room preparation
                    $scope.prepareRoom = function(){
                        console.log('Preparing room!');
                        $scope.$broadcast('prepare');
                    };

                    //Function that triggers leaving a room
                    $scope.leaveRoom = function(){
                        console.log('Leaving room!');
                        $scope.$broadcast('leaveRoom');
                    };

                    //WebRTC Event handlers
                    function rtcEventHandler(webrtc){

                        //When ready to call
                        webrtc.on('readyToCall', function () {});

                        //When video is removed from DOM
                        webrtc.on('videoRemoved', function (video, peer) {
                            var remotes = document.getElementById('remoteVideo');
                            var el = document.getElementById(peer ? 'container_' + webrtc.getDomId(peer) : 'localScreenContainer');
                            if (remotes && el) {
                                remotes.removeChild(el);
                            }
                            console.log('videoRemoved!');
                        });

                        //When video is added to DOM
                        webrtc.on('videoAdded', function (video, peer) {

                            /*
                             *
                             * BROADCAST STARTING
                             *
                             */

                            //Controller callback triggered
                            if($scope.startHandler) {
                                $scope.startHandler();
                            }

                            //Creating remote video element
                            var remotes = document.getElementById('remoteVideo');
                            if (remotes) {
                                var container = document.createElement('div');
                                container.className = 'videoContainer';
                                container.id = 'container_' + webrtc.getDomId(peer);
                                container.appendChild(video);
                                // suppress contextmenu
                                video.oncontextmenu = function () { return false; };
                                remotes.appendChild(container);
                            }

                            //If chat exists, resending chat messages to new peer
                            if(BroadcastService.messages.length > 0){
                                angular.forEach(BroadcastService.messages, function(value){
                                    webrtc.sendToAll('old_chat', {
                                        message_data: value.message,
                                        sender: $scope.userObj.name,
                                        date: value.date
                                    });
                                });
                            }

                            // add muted and paused elements
                            function auxElements(){
                                var muted = document.createElement('span');
                                muted.className = 'muted fa fa-volume-down';
                                container.appendChild(muted);
                                var paused = document.createElement('span');
                                paused.className = 'paused fa fa-pause-circle-o';
                                container.appendChild(paused);
                            }

                            auxElements();

                        });

                        //When WebRTC main API fails
                        webrtc.on('iceFailed', function (peer) {
                            console.error('ice failed', peer);
                        });

                        //When connectivity error is triggered
                        webrtc.on('connectivityError', function (peer) {
                            console.error('connectivity error', peer);
                        });

                        //When a new peer joins the room
                        webrtc.on('joinedRoom', function () {
                            var peers = webrtc.getPeers();
                            //If number of peers is larger than maxNumPeers
                            if (peers && Array.isArray(peers) &&
                                peers.length >= $scope.maxNumPeers) {
                                console.error('Too many people in the room, leaving');
                                webrtc.leaveRoom();
                                webrtc.disconnect();
                                return;
                            }
                        });

                        // listen for mute and unmute events
                        webrtc.on('mute', function (data) {
                            console.log('mute data', data);
                            console.log('showing mute!');// show muted symbol
                            webrtc.getPeers(data.id).forEach(function (peer) {
                                if (data.name == 'audio') {
                                    $('#container_' + webrtc.getDomId(peer) + ' .muted').show();
                                } else if (data.name == 'video') {
                                    $('#container_' + webrtc.getDomId(peer) + ' .muted').hide();
                                    $('#container_' + webrtc.getDomId(peer) + ' .paused').show();
                                    $('#container_' + webrtc.getDomId(peer) + ' video').hide();
                                }
                            });
                        });
                        webrtc.on('unmute', function (data) {
                            console.log('Unmuting'); // hide muted symbol
                            webrtc.getPeers(data.id).forEach(function (peer) {
                                if (data.name == 'audio') {
                                    $('#container_' + webrtc.getDomId(peer) + ' .muted').hide();
                                } else if (data.name == 'video') {
                                    $('#container_' + webrtc.getDomId(peer) + ' .muted').hide();
                                    $('#container_' + webrtc.getDomId(peer) + ' video').show();
                                    $('#container_' + webrtc.getDomId(peer) + ' .paused').hide();
                                }
                            });
                        });

                        //Leave room handler
                        $scope.$on('leaveRoom', function leaveRoom() {
                            BroadcastService.isBroadcasting = false;
                            console.log('leaving $scope.room', BroadcastService.room);
                            if (!BroadcastService.room) {
                                return;
                            }
                            webrtc.leaveRoom();
                            webrtc.disconnect();
                            console.log('messages = ', BroadcastService.messages);
                            $scope.leaveHandler(BroadcastService);
                        });


                        /*
                        *
                        * CHAT MESSAGES
                        *
                        */

                        //Enter key handler
                        $(document).keypress(function(e){
                            var message_area = $('textarea').val();
                            if(e.which == 13 && typeof message_area != 'undefined' && message_area && $.trim($('textarea').val()).length > 0){
                                console.log(message_area);
                                $scope.sendMessage();
                            }
                        });

                        //Send message handler
                        $scope.sendMessage = function(){
                            //Creating message object
                            var message_date = new Date().toLocaleString();
                            console.log(message_date);
                            var message_area = $('textarea').val();
                            if(typeof message_area != 'undefined' && message_area && $.trim($('textarea').val()).length > 0){
                                //Sending data to service
                                BroadcastService.messages.push({"sender":$scope.userObj.name, "message":message_area, "date": message_date});
                                //Sending data to chat
                                webrtc.sendToAll('chat', {
                                    message: message_area,
                                    sender: $scope.userObj.name,
                                    date: message_date,
                                });
                                //Adding text to DOM
                                $('#message-receiver').append('<br>' + $scope.userObj.name + ': ' + message_area);
                                //Clearing data from input
                                $('#message-area').val(' ');
                            }
                        };

                        //When message is received
                        webrtc.connection.on('message', function(data) {
                            //If new incoming chat message
                            if(data.type == 'chat') {
                                console.log('receiving chat', data);
                                //Saving chat data in BroadcastService
                                BroadcastService.messages.push({"sender":data.payload.sender, "message":data.payload.message, "date": data.payload.date});
                                console.log(BroadcastService.messages);
                                //Appending to DOM
                                $('#message-receiver').append('<br>' + data.payload.sender + ':  ' + data.payload.message);
                                //If old_chat
                            } else if (data.type == 'old_chat') {
                                console.log('receiving old chat', data);
                                //Appending to DOM
                                $('#message-receiver').append('<br>' + data.payload.sender + ':  ' + data.payload.message_data);
                            }
                        });

                        /*
                        *
                        *   BUTTONS
                        *
                        */

                        //Mutes audio
                        $scope.mute = function(){
                            webrtc.mute();
                            console.log('Muted!');
                        };
                        //Unmuted audio
                        $scope.unMute = function(){
                            webrtc.unmute();
                            console.log('Unmuted');
                        };
                        //Pause all media
                        $scope.pause = function(){
                            webrtc.pause();
                            console.log('paused');
                        };
                        //Resumes all media
                        $scope.resume = function(){
                            webrtc.resume();
                            console.log('resume');
                        };
                        //Sends invite to receiver with button
                        $scope.sendInvite = function(){
                            if($scope.inviteHandler){
                                $scope.inviteHandler(BroadcastService);
                            }
                        };
                    }
                    
                    //If muted this option will also mute mirror video
                    function postCreateRTCOptions(webrtc){
                        webrtc.config.localVideo.mirror == Boolean($scope.mirror);
                        if($scope.muted){
                            webrtc.mute();
                        }
                    }
                }
            };
        });
})();


