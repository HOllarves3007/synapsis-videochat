'use strict';

(function(){
    angular.module('SynapsisVideoChat.services')
    .service('BroadcastService', [BroadcastService]);

    function BroadcastService(){
        return {
            isBroadcasting:null,
            host: {},
            receiver: {},
            room: null,
            messages: []
        }
    }
})();

