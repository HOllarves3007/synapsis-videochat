'use strict';

(function(){

    angular.module('SynapsisVideoChat.controllers')
        .controller('MainCtrl', ['$scope', '$rootScope','BroadcastService', MainCtrl]);

    function MainCtrl($scope, $rootScope, BroadcastService){
        console.log('MainCtrl!!');
        //assign control to visual model
        var vm = this;

        /*
         *
         * Hard Coded data
         *
         */

        //Doctor dummy array
        vm.doctors =  [
            {
                id: 1,
                name:'Dr. Einstein'
            },
            {
                id: 2,
                name: 'Dr. Xavier'
            },
            {
                id: 3,
                name: 'Dr. Evil'
            }
        ];

        //Logged in user
        $rootScope.myUser = {
            id: '1',
            name: 'Henry'
        };

        //Hard coded host data
        vm.host = {
            id: '1',
            name: 'Henry'
        };

        //Hard coded receiver data
        vm.myReceiver = {
            id: '5',
            name: 'Guillermo'
        };

        //Switches and directive parameters
        $rootScope.isBroadcasting = false;
        //$rootScope.minWidth = 200;
        $rootScope.maxNumPeers = 2;

        //If trigerred by host
        $scope.startBroadcast = function(receiver){
            BroadcastService.host = $scope.myUser;
            BroadcastService.receiver = receiver;
            BroadcastService.isBroadcasting = true;
            //passing BroadcastService to $rootScope
            $rootScope.broadcastService = BroadcastService;
            console.log('startBroadcast', $rootScope.broadcastService.isBroadcasting);

        };

        //If received by invitation
        $scope.joinBroadcast = function (host) {
            BroadcastService.host = host;
            BroadcastService.receiver = $scope.myUser;
            BroadcastService.isBroadcasting = true;
            $rootScope.broadcastService = BroadcastService;
            console.log('joinBroadcast', $rootScope.broadcastService);
        };

        /*
         *
         * DIRECTIVE CALLBACKS
         *
         */

        //When invite is sent
        $rootScope.invite = function(data){
            console.log('Invite sent!', data);
        };
        //When broadcast room is left
        $rootScope.leave = function(data){
            console.log('Left! Here are the messages = ', data);
        };
        //It's pretty obvious
        $rootScope.broadcastStart = function(){
            console.log('Start counting!');
        };
    }

})();
